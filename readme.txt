/* module-specific examples */

// hook_search
function hook_search($op = 'search', $keys = null) {
  switch ($op) {
    case 'name':
      return t('Example');
      break;

    case 'search':
      $results = array();
      $rows = 20;
      $items = solr_search_solr('example', $keys, $_GET['page'], $rows);

      foreach ($items as $item){
        $results[] = array(
          'link' => url('node/'. $item['nid'], NULL, NULL, TRUE),
          'title' => $item['title'],
          'snippet' => search_excerpt($keys, $item['body']) // FIXME: use Solr Highlighter
        );
      }

      return $results;
      break;
  }
}

// called by module_invoke_all('solr')
function hook_solr($op, $type, $node){
  // limit to this type of node
  if ($type != 'example') continue;
  
  switch ($op){
    // data to be posted to Solr for a node to be indexed
    case 'data':
      return sprintf(
        '<add><doc><field name="nid">%s</field><field name="title">%s</field><field name="body">%s</field></doc></add>',
        $node->nid,
        htmlspecialchars($node->title, ENT_QUOTES),
        htmlspecialchars($node->body, ENT_QUOTES)
      );
      
    // query handler to use for nodes of this type
    case 'qt':
      return 'dismax';
  }
}

// called when nodes are updated
function hook_update_index(){
  solr_update_solr('example');
}
